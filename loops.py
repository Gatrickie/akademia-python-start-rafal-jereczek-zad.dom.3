#!/usr/bin/python3
import random

# While loop

x: int = 0

while x != 20:
    x = random.randrange(10, 30, 1)
    print(x)

print("Found 20!")

# For loop

for x in range(20, -1, -1):
    print(x)

print("End of loop")

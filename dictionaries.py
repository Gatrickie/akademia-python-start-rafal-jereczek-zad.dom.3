#!/usr/bin/env python3

# Dictionary with 5 elements

first_key = input("First key: ")
first_value = input("First value: ")
second_key = input("Second key: ")
second_value = input("Second value: ")
third_key = input("Third key: ")
third_value = input("Third value: ")
fourth_key = input("Fourth key: ")
fourth_value = input("Fourth value: ")
fifth_key = input("Fifth key: ")
fifth_value = input("Fifth value: ")

dictionary = {first_key: first_value, second_key: second_value,
              third_key: third_value, fourth_key: fourth_value,
              fifth_key: fifth_value}

print(dictionary)

# Dictionary with 5 lists

things = {"flower": ["Rose", "Dandelion", "Tulip", "Sunflower", "Orchid"],
          "animal": ["Dog", "Cat", "Horse", "Donkey", "Chicken"],
          "furniture": ["Chair", "Table", "Armchair", "Wardrobe", "Couch"],
          "name": ["Damian", "Paul", "Martin", "George", "Franklin"],
          "dishes": ["Fries", "Nuggets", "Pizza", "Hamburger", "Sandwich"]}

print(things)
print(things["flower"])
print(things["name"])

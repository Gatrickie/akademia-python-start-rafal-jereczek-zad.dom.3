#!/usr/bin/env python3

# Create tuple with 100 elements

first = tuple(range(100, 200))
print(first)
print("Number of elements: ", len(first))

print("-" * 20)
# Create tuple with 5 elements and assign each of them to different variable

u, w, x, y, z = ("Audi", "VW", "Opel", "Seat", "Skoda")
print("u = ", u)
print("w = ", w)
print("x = ", x)
print("y = ", y)
print("z = ", z)

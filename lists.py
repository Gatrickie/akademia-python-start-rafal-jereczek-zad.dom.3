#!/usr/bin/env python3

# List with tree lists inside
family = [["mom"], ["dad"], ["grandma"]]
print(family)
print(family[0])
print(family[1])
print(family[2])

print("-" * 20)

# Empty list filled with for loop
numbers = list()

for num in range(100, 116, 1):
    numbers.append(num)

print(numbers)

print("-" * 20)

# List with 5 elements and print it with for loop
five = [1.123, 2.234, 3.345, 4.456, 5.567]
print(five)

for num in range(5):
    print(five[num], "-", type(five[num]))
